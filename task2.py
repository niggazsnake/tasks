# -*- coding: utf-8 -*-
def convert(x):
    priznak = 0
    if x < 0: priznak = 1
    x = abs(x)
    new_number = 0
    cnt = count_numbers(x)
    while x > 0:
        new_number += 10 ** (cnt - 1) * (x % 10)
        cnt -= 1
        x = x / 10
    if new_number > 2147483647 or new_number < -2147483647:
        return 0
    if priznak:
        new_number = new_number * (-1)

    return new_number

def count_numbers(x):
    cnt = 0
    while x > 0:
        x = x / 10
        cnt += 1
    return cnt

print convert(-3123123234525234523452345234534523453452342342342312)