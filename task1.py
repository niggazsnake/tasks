# -*- coding: utf-8 -*-
def convert(s, numRows):
    """
        вернуть строку ввиде зигзага
    """
    strs = dict((i, "") for i in xrange(numRows))
    iter = 0
    p = 0
    for i in xrange(0, len(s)):

        print s[i], i, iter
        strs[iter] = strs[iter] + s[i]

        if p == 1 and iter == 0:
            p = 0

        if (iter + 1) == numRows or p == 1:
            iter -= 1
            if iter < 0: iter = 0
            p = 1
        else:
            iter += 1

    res = ""

    print strs

    for i in xrange(0, len(strs)):
        res += strs[i]

    return res


print convert("ABC", 2)